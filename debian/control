Source: princeprocessor
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Sascha Steinbiss <satta@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13), help2man
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/pkg-security-team/princeprocessor
Vcs-Git: https://salsa.debian.org/pkg-security-team/princeprocessor.git
Homepage: https://github.com/hashcat/princeprocessor

Package: princeprocessor
Architecture: amd64 arm64 mips64el ppc64el s390x alpha kfreebsd-amd64 ppc64 riscv64 sparc64 x32
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: standalone password candidate generator using the PRINCE algorithm
 Princeprocessor is a password candidate generator and can be thought of
 as an advanced combinator attack. Rather than taking as input two different
 wordlists and then outputting all the possible two word combinations though,
 princeprocessor only has one input wordlist and builds "chains" of combined
 words. These chains can have 1 to N words from the input wordlist
 concatenated together.
 The name PRINCE is used as an acronym and stands for PRobability INfinite
 Chained Elements, which are the building blocks of the algorithm.
